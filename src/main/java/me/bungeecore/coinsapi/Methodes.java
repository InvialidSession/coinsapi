package me.bungeecore.coinsapi;

import lombok.Getter;

@Getter
public class Methodes {

    private final String prefix;
    private final String noperm;

    public Methodes() {
        this.prefix = "§8[§aCoins§8] ";
        this.noperm = prefix + "§cDu bist nicht berechtigt diesen Befehl auszuführen.";
    }
}
