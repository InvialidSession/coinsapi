package me.bungeecore.coinsapi.spigot;

import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class CoinsAPI extends JavaPlugin {

    @Getter
    private static CoinsAPI instance;

    @Override
    public void onEnable() {
        instance = this;
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}