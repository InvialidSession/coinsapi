package me.bungeecore.coinsapi.bungee.commands;

import me.bungeecore.coinsapi.bungee.CoinsAPI;
import me.bungeecore.coinsapi.utils.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class CoinsCommand extends Command {

    private CoinsAPI instance;

    public CoinsCommand(String name, CoinsAPI instance) {
        super(name);
        this.instance = instance;
        ProxyServer.getInstance().getPluginManager().registerCommand(instance, this);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;

            if (args.length == 0) {
                player.sendMessage(instance.getMethodes().getPrefix() + "§7Du hast §a" + instance.getMongoManager().getCoinsCache().get(player.getUniqueId().toString()).getCoins() + " §7Coins.");
                return;
            } else {
                if (player.hasPermission("coins.admin")) {
                    if (args.length == 2) {
                        if (args[0].equalsIgnoreCase("see")) {
                            String name = args[1];
                            UUID uuid = UUIDFetcher.getUUID(name);
                            if (uuid == null) {
                                player.sendMessage(instance.getMethodes().getPrefix() + "§cDieser Spieler existiert nicht.");
                                return;
                            }
                            if (!instance.getMongoManager().getCoinsCache().containsKey(uuid.toString())) {
                                instance.getMongoManager().loadPlayer(uuid.toString());
                            }
                            ProxyServer.getInstance().getScheduler().schedule(instance, () -> {
                                long coins = instance.getMongoManager().getCoinsCache().get(uuid.toString()).getCoins();

                                player.sendMessage(instance.getMethodes().getPrefix() + "§7Der Spieler §a" + name + " §7hat §a" + coins + " §7Coins.");

                                if (ProxyServer.getInstance().getPlayer(name) == null) {
                                    instance.getMongoManager().getCoinsCache().remove(uuid.toString());
                                }
                            }, 2, TimeUnit.SECONDS);
                            return;
                        } else {
                            sendHelpMessage(commandSender);
                            return;
                        }
                    } else if (args.length == 3) {
                        String name = args[1];
                        UUID uuid = UUIDFetcher.getUUID(name);
                        if (args[0].equalsIgnoreCase("add")) {
                            try {
                                instance.getMongoManager().addAndRemoveCoins(uuid.toString(), Long.valueOf(args[2]), true);
                            } catch (NumberFormatException ex) {
                                player.sendMessage(instance.getMethodes().getPrefix() + "§cDu musst eine Nummer angeben.");
                                return;
                            }

                            player.sendMessage(instance.getMethodes().getPrefix() + "§7Du hast §aerfolgreich §7dem Spieler §a" + name + " §a" + args[2] + " §7Coins hinzugefügt.");
                        } else if (args[0].equalsIgnoreCase("remove")) {
                            try {
                                instance.getMongoManager().addAndRemoveCoins(uuid.toString(), Long.valueOf(args[2]), false);
                            } catch (NumberFormatException ex) {
                                player.sendMessage(instance.getMethodes().getPrefix() + "§cDu musst eine Nummer angeben.");
                                return;
                            }

                            player.sendMessage(instance.getMethodes().getPrefix() + "§7Du hast §aerfolgreich §7dem Spieler §a" + name + " §a" + args[2] + " §7Coins entfernt.");
                        } else {
                            sendHelpMessage(commandSender);
                            return;
                        }
                    } else {
                        sendHelpMessage(commandSender);
                        return;
                    }
                } else {
                    player.sendMessage(instance.getMethodes().getNoperm());
                    return;
                }
            }
        } else {
            if (args.length == 0) {
                sendHelpMessage(commandSender);
                return;
            }
            if (args.length == 2) {
                String name = args[1];
                UUID uuid = UUIDFetcher.getUUID(name);
                if (args[0].equalsIgnoreCase("see")) {
                    if (uuid == null) {
                        commandSender.sendMessage("Dieser Spieler existiert nicht.");
                        return;
                    }
                    if (!instance.getMongoManager().getCoinsCache().containsKey(uuid.toString())) {
                        instance.getMongoManager().loadPlayer(uuid.toString());
                    }
                    ProxyServer.getInstance().getScheduler().schedule(instance, () -> {
                        long coins = instance.getMongoManager().getCoinsCache().get(uuid.toString()).getCoins();

                        commandSender.sendMessage(name + " hat " + coins + " Coins.");

                        if (ProxyServer.getInstance().getPlayer(name) == null) {
                            instance.getMongoManager().getCoinsCache().remove(uuid.toString());
                        }
                    }, 2, TimeUnit.SECONDS);
                } else {
                    sendHelpMessage(commandSender);
                    return;
                }
            }
            if (args.length == 3) {
                String name = args[1];
                UUID uuid = UUIDFetcher.getUUID(name);
                if (args[0].equalsIgnoreCase("add")) {
                    if (uuid == null) {
                        commandSender.sendMessage("Dieser Spieler existiert nicht.");
                        return;
                    }
                    try {
                        instance.getMongoManager().addAndRemoveCoins(UUIDFetcher.getUUID(name).toString(), Long.valueOf(args[2]), true);
                    } catch (NumberFormatException ex) {
                        commandSender.sendMessage("Du musst eine Nummer eingeben.");
                        return;
                    }

                    commandSender.sendMessage("Du hast dem Spieler " + name + " " + args[2] + " Coins gegeben.");
                } else if (args[0].equalsIgnoreCase("remove")) {
                    if (uuid == null) {
                        commandSender.sendMessage("Dieser Spieler existiert nicht.");
                        return;
                    }
                    try {
                        instance.getMongoManager().addAndRemoveCoins(UUIDFetcher.getUUID(name).toString(), Long.valueOf(args[2]), false);
                    } catch (NumberFormatException ex) {
                        commandSender.sendMessage("Du musst eine Nummer eingeben.");
                        return;
                    }
                    commandSender.sendMessage("Du hast dem Spieler " + name + " " + args[2] + " Coins entfernt.");
                } else {
                    sendHelpMessage(commandSender);
                    return;
                }
            }
        }
    }

    private void sendHelpMessage(CommandSender commandSender) {
        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;

            player.sendMessage(instance.getMethodes().getPrefix() + "§cUsage: /coins <add|remove|see> <Name> <coins>");
        } else {
            commandSender.sendMessage("Usage: /coins <add|remove|see> <Name> <coins>");
        }
    }
}