package me.bungeecore.coinsapi.bungee.listener;

import me.bungeecore.coinsapi.bungee.CoinsAPI;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerDisconnectListener implements Listener {

    private CoinsAPI instance;

    public PlayerDisconnectListener(CoinsAPI instance) {
        this.instance = instance;
        ProxyServer.getInstance().getPluginManager().registerListener(instance, this);
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent event) {
        ProxiedPlayer player = event.getPlayer();

        instance.getMongoManager().updateCoins(player.getUniqueId().toString());
        instance.getMongoManager().getCoinsCache().remove(player.getUniqueId().toString());
    }
}
