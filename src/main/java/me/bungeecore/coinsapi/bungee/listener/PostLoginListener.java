package me.bungeecore.coinsapi.bungee.listener;

import me.bungeecore.coinsapi.bungee.CoinsAPI;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PostLoginListener implements Listener {

    private CoinsAPI instance;

    public PostLoginListener(CoinsAPI instance) {
        this.instance = instance;
        ProxyServer.getInstance().getPluginManager().registerListener(instance, this);
    }

    @EventHandler
    public void onPostLogin(PostLoginEvent event) {
        ProxiedPlayer player = event.getPlayer();

        instance.getMongoManager().loadPlayer(player.getUniqueId().toString());
    }
}
