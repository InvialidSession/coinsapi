package me.bungeecore.coinsapi.bungee;

import lombok.Getter;
import me.bungeecore.coinsapi.Methodes;
import me.bungeecore.coinsapi.bungee.commands.CoinsCommand;
import me.bungeecore.coinsapi.bungee.listener.PlayerDisconnectListener;
import me.bungeecore.coinsapi.bungee.listener.PostLoginListener;
import me.bungeecore.coinsapi.database.MongoManager;
import net.md_5.bungee.api.plugin.Plugin;

@Getter
public class CoinsAPI extends Plugin {

    @Getter
    private static CoinsAPI instance;

    private Methodes methodes;
    private MongoManager mongoManager;

    @Override
    public void onEnable() {
        instance = this;
        init();
    }

    private void init() {
        methodes = new Methodes();

        mongoManager = new MongoManager("localhost", 27017);
        mongoManager.connect("admin", "test123", "admin");

        new CoinsCommand("coins", instance);
        new PostLoginListener(instance);
        new PlayerDisconnectListener(instance);
    }

    @Override
    public void onDisable() {

    }
}
