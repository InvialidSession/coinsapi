package me.bungeecore.coinsapi.database;

import com.google.gson.Gson;
import com.mongodb.ConnectionString;
import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;

import com.mongodb.client.model.Filters;
import lombok.Getter;
import me.bungeecore.coinsapi.utils.CoinsPlayer;
import net.md_5.bungee.api.ProxyServer;
import org.bson.Document;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

@Getter
public class MongoManager {

    private MongoClient mongoClient;
    private MongoDatabase mongoDatabase;

    private MongoCollection<Document> coins;

    private Map<String, CoinsPlayer> coinsCache;
    private Gson gson;

    private Timer timer;

    private final String hostname;
    private final int port;

    public MongoManager(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;

        gson = new Gson();
        coinsCache = new HashMap<>();
        timer = new Timer();
    }

    public void connect(String username, String password, String database) {
        this.mongoClient = MongoClients.create(new ConnectionString(MessageFormat.format("mongodb://{0}:{1}@{2}:{3}/{4}", username, password, hostname, String.valueOf(port), database)));
        this.mongoDatabase = this.mongoClient.getDatabase("players");

        this.coins = this.mongoDatabase.getCollection("coins");
    }

    public boolean loadPlayer(String uuid) {
        getCoins().find(new Document("uuid", uuid)).first((doc, throwable) -> {
            if (doc == null) {
                Document document = new Document("uuid", uuid).append("coins", Long.valueOf("0"));

                getCoins().insertOne(document, (aVoid, throwable1) -> {});
                getCoinsCache().put(uuid,new CoinsPlayer(uuid, 0));
                return;
            }
            if (!getCoinsCache().containsKey(uuid)) {
                getCoinsCache().put(uuid, new CoinsPlayer(uuid, doc.getLong("coins")));
            } else {
                getCoinsCache().remove(uuid);
                getCoinsCache().put(uuid, new CoinsPlayer(uuid, doc.getLong("coins")));
            }
        });
        return getCoinsCache().containsKey(uuid);
    }

    public void updateCoins(String uuid) {
        getCoins().find(new Document("uuid", uuid)).first((doc, throwable) -> {
            if (doc != null) {
                CoinsPlayer coinsPlayer = getCoinsCache().get(uuid);

                Document document = getGson().fromJson(getGson().toJson(coinsPlayer), Document.class);

                getCoins().replaceOne(Filters.eq("uuid", uuid), document, (updateResult, throwable1) -> {});
            }
        });
    }

    public void addAndRemoveCoins(String uuid, long coins, boolean add) {
        if (!getCoinsCache().containsKey(uuid)) {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    getCoinsCache().get(uuid).setCoins((add ? (getCoinsCache().get(uuid).getCoins() + coins) : (getCoinsCache().get(uuid).getCoins() - coins)));
                    updateCoins(uuid);

                    if (ProxyServer.getInstance().getPlayer(uuid) == null) {
                        getCoinsCache().remove(uuid);
                    }
                }
            }, 2000);
        } else {
            getCoinsCache().get(uuid).setCoins((add ? (getCoinsCache().get(uuid).getCoins() + coins) : (getCoinsCache().get(uuid).getCoins() - coins)));

            updateCoins(uuid);
        }
    }
}