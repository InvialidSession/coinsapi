package me.bungeecore.coinsapi.utils;

import lombok.Data;

@Data
public class CoinsPlayer {

    private String uuid;
    private long coins;

    public CoinsPlayer(String uuid, long coins) {
        this.uuid = uuid;
        this.coins = coins;
    }
}
